class Hero < ActiveRecord::Base
  mount_uploader :image, ImageUploader

  has_many :reviews
  
  def self.search(search)
    where("name LIKE ?", "%#{search}%")
  end

  validates :name, :main_att, :str_base, :int_base, :agi_base, :image, :coment, presence: true
  validates :str_base, :agi_base, :int_base, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 1,
    less_than_or_equal_to: 100,
    message: "Apenas números entre 0 e 100"
  }
end
