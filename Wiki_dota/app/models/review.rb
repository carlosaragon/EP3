class Review < ActiveRecord::Base
  belongs_to :user
  belongs_to :hero

  validates :rating, :comment, presence: true
  validates :rating, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 1,
    less_than_or_equal_to: 5,
    message: "Apenas números entre 0 e 5"
  }

end
