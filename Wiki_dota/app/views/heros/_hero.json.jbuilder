json.extract! hero, :id, :name, :main_att, :str_base, :int_base, :agi_base, :coment, :created_at, :updated_at
json.url hero_url(hero, format: :json)
